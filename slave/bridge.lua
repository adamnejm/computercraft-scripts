
local args = {...}
if #args < 1 then
	print("Usage: bridge <length>")
end

-------------------------------------------

local function select_item()
	for i = 1, 16 do
		if turtle.getItemDetail(i) then
			return turtle.select(i)
		end
	end
	return false
end

for _ = 1, tonumber(args[1]) do
	if not select_item() then
		print("Out of blocks!")
		error()
	end
	
	if not turtle.forward() then
		print("Reached an obstacle!")
		error()
	end
	
	if not turtle.placeDown() and not turtle.detectDown() then
		print("Cannot place this block!")
		error()
	end
end

print("Done!")
