
--- Precious blocks that are worth way more mined manually with a Fortune III pickaxe
--- Turtle will wait until these are mined manually before progressing
local halt_blocks = {
	["minecraft:diamond_ore"]           = true,
	["minecraft:deepslate_diamond_ore"] = true,
	-- ["minecraft:iron_ore"]              = true,
	-- ["minecraft:deepslate_iron_ore"]    = true,
	-- ["minecraft:gold_ore"]              = true,
	-- ["minecraft:deepslate_gold_ore"]    = true,
}

-------------------------------------------

local args = { ... }
if #args < 3 then
	print("Usage: tunnel <width> <height> <depth>")
	print("Place the turtle on left-bottom corner, 1 block in front of the tunnel")
	return
end

local width, height, depth = tonumber(args[1]), tonumber(args[2]), tonumber(args[3])
if not width or not height or not depth then
	print("Unable to parse arguments to numbers!")
	return
end


-------------------------------------------
--                ACTIONS                --
-------------------------------------------

local function is_halt_block(inspect_func)
	local exists, data = inspect_func()
	return exists and halt_blocks[data.name]
end

local function dig(dig_func, inspect_func)
	if is_halt_block(inspect_func) then
		print("Precious block found! Remove it manually to continue...")
		repeat sleep(1) until not is_halt_block(inspect_func)
	end
	return dig_func()
end
local function digUp() return dig(turtle.digUp, turtle.inspectUp) end
local function digDown() return dig(turtle.digDown, turtle.inspectDown) end
local function digForward() return dig(turtle.dig, turtle.inspect) end

local function forward()
	while not turtle.forward() do
		if not dig(turtle.dig, turtle.inspect) then
			error("Unbreakable block encountered! Exiting...")
		end
	end
end


-------------------------------------------
--               TUNNELING               --
-------------------------------------------

local function initialize_tunnel()
	if height >= 3 then
		turtle.up()
	end
	
	digForward()
	forward()
	
	if width >= 2 then
		turtle.turnRight()
	end
end

local function dig_line(should_turn)
	for i = 1, width do
		if height >= 2 then digUp() end
		if height >= 3 then digDown() end
		
		if i < width then
			digForward()
			forward()
		elseif should_turn and width >= 2 then
			turtle.turnRight()
			turtle.turnRight()
		end
	end
end

local function dig_slice(up)
	local h = math.ceil(height / 3)
	for i = 1, h do
		dig_line(i < h)
		
		if i < h then
			for _ = 1, math.min(3, height - i * 3) do
				if up then
					turtle.up()
					digUp()
				else
					turtle.down()
					digDown()
				end
			end
		end
	end
end

local function dig_tunnel()
	for i = 1, depth do
		local up = i % 2 ~= 0
		dig_slice(up)
		
		if i < depth then
			if width < 2 then
				digForward()
				forward()
			else
				local swap = (math.ceil(height / 3) * i) % 2 == 0
				local turn_func = swap and turtle.turnRight or turtle.turnLeft
				turn_func()
				digForward()
				forward()
				turn_func()
			end
		end
	end
end

-------------------------------------------

initialize_tunnel()
dig_tunnel()

print("Finished tunneling!")
