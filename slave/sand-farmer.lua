

local function select_item(name)
	for i = 1, 16 do
		local data = turtle.getItemDetail(i)
		if data and data.name == name then
			turtle.select(i)
			return i
		end
	end
end

while not turtle.getItemDetail(16) do
	
	turtle.up()
	turtle.up()
	
	if not select_item("minecraft:anvil") then
		break
	end
	
	turtle.placeDown()
	
	turtle.down()
	turtle.digDown()
	
	turtle.down()
	turtle.digDown()
	
end



