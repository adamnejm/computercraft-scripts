
local move      = require "lib.move"
local path      = require "lib.path"
local tutil     = require "lib.tutil"
local inventory = require "lib.inventory"
local Vector    = require "lib.vector"

-------------------------------------------

-- This kinda depends on the Fast Leaf Decay mod, because it cuts down the tree and immediately checks for dropped saplings
-- To make it work without it, simply first cut down all the trees or some specific amount, then traverse area to look for saplings and replant

local home = Vector(2127, 63, 1401)
local fuel_needed = 1000

-- Location of trees
local trees = {
	Vector(2133, 63, 1401), Vector(2136, 63, 1401), Vector(2139, 63, 1401), Vector(2142, 63, 1401), Vector(2145, 63, 1401), Vector(2148, 63, 1401), Vector(2151, 63, 1401), Vector(2154, 63, 1401),
	Vector(2154, 63, 1404), Vector(2151, 63, 1404), Vector(2148, 63, 1404), Vector(2145, 63, 1404), Vector(2142, 63, 1404), Vector(2139, 63, 1404), Vector(2136, 63, 1404), Vector(2133, 63, 1404),
	Vector(2133, 63, 1407), Vector(2136, 63, 1407), Vector(2139, 63, 1407), Vector(2142, 63, 1407), Vector(2145, 63, 1407), Vector(2148, 63, 1407), Vector(2151, 63, 1407), Vector(2154, 63, 1407),
	Vector(2154, 63, 1410), Vector(2151, 63, 1410), Vector(2148, 63, 1410), Vector(2145, 63, 1410), Vector(2142, 63, 1410), Vector(2139, 63, 1410), Vector(2136, 63, 1410), Vector(2133, 63, 1410),
	Vector(2133, 63, 1413), Vector(2136, 63, 1413), Vector(2139, 63, 1413), Vector(2142, 63, 1413), Vector(2145, 63, 1413), Vector(2148, 63, 1413), Vector(2151, 63, 1413), Vector(2154, 63, 1413),
	Vector(2154, 63, 1416), Vector(2151, 63, 1416), Vector(2148, 63, 1416), Vector(2145, 63, 1416), Vector(2142, 63, 1416), Vector(2139, 63, 1416), Vector(2136, 63, 1416), Vector(2133, 63, 1416),
	Vector(2133, 63, 1419), Vector(2136, 63, 1419), Vector(2139, 63, 1419), Vector(2142, 63, 1419), Vector(2145, 63, 1419), Vector(2148, 63, 1419), Vector(2151, 63, 1419), Vector(2154, 63, 1419),
	Vector(2154, 63, 1422), Vector(2151, 63, 1422), Vector(2148, 63, 1422), Vector(2145, 63, 1422), Vector(2142, 63, 1422), Vector(2139, 63, 1422), Vector(2136, 63, 1422), Vector(2133, 63, 1422),
}

-------------------------------------------

local function chop_tree()
	turtle.dig()
	turtle.forward()
	
	while turtle.detectUp() do
		turtle.digUp()
		turtle.up()
	end
end

local function plant_tree(tree_position)
	if not inventory.select("sapling") then return end
	tutil.place_block(tree_position)
end

local function process_tree(tree_position)
	if not path.go_to_block(tree_position) then return end
	if not move.face(tree_position) then return end
	
	local block_exists, block_data = tutil.inspect_block(tree_position)
	if not block_exists then
		plant_tree(tree_position)
	elseif string.find(block_data.name, "log") then
		chop_tree()
		if not path.go_to_block(tree_position) then return end
		plant_tree(tree_position)
	end
end

-------------------------------------------

local function escape()
	-- Sometimes tree grows funny
	for i = 1, 20 do
		turtle.digUp()
		turtle.up()
	end
end

local furnaces = { "minecraft:furnace_3", "minecraft:furnace_4" }
local function deposit()
	if not path.go_to(home, 1) then
		escape()
		if not path.go_to(home, 0) then
			error("Cannot find a way home!")
		end
	end
	
	-- Deposit all wood and saplings
	local wood_chest = inventory.connect(34)
	local sapling_chest = inventory.connect(35)
	wood_chest:send("log")
	sapling_chest:send("sapling")
	
	-- Grab sticks and drop all junk
	sapling_chest:pull("stick")
	inventory.drop()
	
	-- Grab at most 64 wood and coal per furnace
	local coal_chest = inventory.connect(33)
	wood_chest:pull("log", nil, #furnaces * 64)
	coal_chest:pull("coal", nil, #furnaces * 64)
	
	-- Grab smelted charcoal from furnace, refill furnace with wood and coal
	for _, furnace_id in ipairs(furnaces) do
		local furnace = inventory.connect(furnace_id)
		furnace:pull(3)
		furnace:send("log", 1)
		furnace:send("coal", 2)
	end
	
	-- Deposit coal and wood again
	wood_chest:send("log")
	coal_chest:send("coal")
	
	-- Refuel if needed
	coal_chest:pull("coal", nil, inventory.get_fuel_needed(fuel_needed))
	inventory.refuel()
	
	-- Grab saplings
	sapling_chest:pull("sapling", nil, #trees)
end

-------------------------------------------

while true do
	deposit()
	
	for _, tree_position in ipairs(trees) do
		process_tree(tree_position)
	end
	
	deposit()
	
	-- Smelting a stack takes just a little over 10 minutes, so this is the most optimal:
	-- sleep(600)
	
	-- This looks cooler though
	sleep(60)
end
