
local move      = require "lib.move"
local path      = require "lib.path"
local tutil     = require "lib.tutil"
local inventory = require "lib.inventory"
local Vector    = require "lib.vector"

-------------------------------------------

-- Target fuel level
local fuel_needed = 2500

-- Deposit hub
local home = Vector(2127, 63, 1401)

-- IDs of inventories connected to the hub
local furnaces         = { "minecraft:furnace_3", "minecraft:furnace_4" }
local coal_chest_id    = "minecraft:chest_33"
local wood_chest_id    = "minecraft:chest_34"
local sapling_chest_id = "minecraft:chest_35"

-- Location of trees
local trees = {
	Vector(2134, 63, 1402), Vector(2139, 63, 1402), Vector(2144, 63, 1402), Vector(2149, 63, 1402), Vector(2154, 63, 1402),
	Vector(2154, 63, 1407), Vector(2149, 63, 1407), Vector(2144, 63, 1407), Vector(2139, 63, 1407), Vector(2134, 63, 1407),
	Vector(2134, 63, 1412), Vector(2139, 63, 1412), Vector(2144, 63, 1412), Vector(2149, 63, 1412), Vector(2154, 63, 1412),
	Vector(2154, 63, 1417), Vector(2149, 63, 1417), Vector(2144, 63, 1417), Vector(2139, 63, 1417), Vector(2134, 63, 1417),
	Vector(2134, 63, 1422), Vector(2139, 63, 1422), Vector(2144, 63, 1422), Vector(2149, 63, 1422), Vector(2154, 63, 1422),
}

-------------------------------------------

local function chop_tree()
	turtle.dig()
	turtle.forward()
	
	while turtle.detectUp() do
		turtle.digUp()
		turtle.up()
	end
	
	-- A little optimization, otherwise turtle will often place the sapling underneath
	-- Resulting in having to pathfind through leaves which may take long if the forest is dense
	while not turtle.detectDown() do
		turtle.down()
	end
	turtle.forward()
end

local function plant_tree(tree_position)
	if not inventory.select("sapling") then return end
	tutil.place_block(tree_position)
end

local function escape()
	for _ = 1, 20 do
		turtle.digUp()
		turtle.up()
	end
end

local function process_tree(tree_position)
	if not path.go_to_block(tree_position) then return escape() end
	
	local block_exists, block_data = tutil.inspect_block(tree_position)
	if not block_exists then
		plant_tree(tree_position)
	elseif string.find(block_data.name, "log") then
		chop_tree()
		if not path.go_to_block(tree_position) then return end
		plant_tree(tree_position)
	end
end

local function process_all_trees()
	for _, tree_position in ipairs(trees) do
		process_tree(tree_position)
	end
end

-------------------------------------------

local function deposit()
	path.go_to(home, 1)
	
	local coal_chest    = inventory.connect(coal_chest_id)
	local wood_chest    = inventory.connect(wood_chest_id)
	local sapling_chest = inventory.connect(sapling_chest_id)
	
	-- Deposit all wood and saplings
	wood_chest:send("log")
	sapling_chest:send("sapling")
	
	-- Grab sticks and drop all junk
	sapling_chest:pull("stick")
	inventory.drop()
	
	-- Grab at most 64 wood and coal per furnace
	wood_chest:pull("log", nil, #furnaces * 64)
	coal_chest:pull("coal", nil, #furnaces * 64)
	
	-- Grab smelted charcoal from furnace, refill furnace with wood and coal
	for _, furnace_id in ipairs(furnaces) do
		local furnace = inventory.connect(furnace_id)
		furnace:pull(3)
		furnace:send("log", 1)
		furnace:send("coal", 2)
	end
	
	-- Deposit coal and wood again
	wood_chest:send("log")
	coal_chest:send("coal")
	
	-- Refuel if needed
	coal_chest:pull("coal", nil, inventory.get_fuel_needed(fuel_needed))
	inventory.refuel()
	
	-- Grab saplings
	sapling_chest:pull("sapling", nil, #trees)
end

-------------------------------------------

while true do
	deposit()
	process_all_trees()
	deposit()
	
	-- Most fuel efficient would be ~10 minutes, because that's how long it takes to smelt a stack
	sleep(60)
end
