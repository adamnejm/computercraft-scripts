> ### <mark>ARCHIVED!</mark>
> <mark>**Current repository at Codeberg: https://codeberg.org/adamnejm/computercraft-scripts**

# ComputerCraft Scripts

Collection of my scripts for [CC: Tweaked](https://github.com/cc-tweaked/CC-Tweaked).  

## Libraries
These scripts do not use a datapack, because they're problematic with symlinks and require `/reload` for every change.  
Instead each computer-scripts folder keeps a symlink to `lib` from the root directory.

## Setup
To link scripts with a computer, symlink the chosen directory to `computercraft/computer/<computer-id>`. Example:
```sh
ln -rs \
~/projects/lua/computercraft-scripts/farmer \
~/.local/share/PrismLauncher/instances/Instance/.minecraft/saves/Flat/computercraft/computer/10
```

## Submodules
Lazy - https://gitlab.com/adamnejm/lazy-lua  
Vector - https://gitlab.com/adamnejm/vector-lua

## Documentation
Every user-exposed library function is documented using [sumneko's LSP](https://github.com/LuaLS/lua-language-server) annotation system.
Meaning you can get dynamic syntax checking and autocompletion in [any code editor](https://microsoft.github.io/language-server-protocol/implementors/tools/) that supports the Language Server Protocol.
