
local move      = require "lib.move"
local path      = require "lib.path"
local tutil     = require "lib.tutil"
local inventory = require "lib.inventory"
local Vector    = require "lib.vector"

-------------------------------------------

-- Target fuel level
local fuel_needed = 2500

-- Deposit hub
local home = Vector(2127, 0, 1401)

-- IDs of inventories connected to the hub
local furnaces = { "minecraft:furnace_0", "minecraft:furnace_1" }
local coal_chest_id = "minecraft:chest_0"
local wood_chest_id = "minecraft:chest_1"
local sapling_chest_id = "minecraft:chest_2"

-- Location of trees
local trees = {
	Vector(2135, 0, 1405), Vector(2140, 0, 1402), Vector(2144, 0, 1405),
	Vector(2149, 0, 1403), Vector(2152, 0, 1407), Vector(2151, 0, 1413),
	Vector(2152, 0, 1419), Vector(2147, 0, 1419), Vector(2141, 0, 1421),
	Vector(2140, 0, 1416), Vector(2135, 0, 1419), Vector(2136, 0, 1412),
}

-------------------------------------------

local function chop_tree()
	turtle.dig()
	turtle.forward()
	
	while turtle.detectUp() do
		turtle.digUp()
		turtle.up()
	end
end

local function plant_tree(tree_position)
	if not inventory.select("sapling") then return end
	tutil.place_block(tree_position)
end

local function process_tree(tree_position)
	if not path.go_to_block(tree_position) then return end
	if not move.face(tree_position) then return end
	
	local block_exists, block_data = tutil.inspect_block(tree_position)
	if not block_exists then
		plant_tree(tree_position)
	elseif string.find(block_data.name, "log") then
		chop_tree()
		if not path.go_to_block(tree_position) then return end
		plant_tree(tree_position)
	end
end

-------------------------------------------

local function deposit()
	path.go_to(home, 1)
	
	local coal_chest = inventory.connect(coal_chest_id)
	local wood_chest = inventory.connect(wood_chest_id)
	local sapling_chest = inventory.connect(sapling_chest_id)
	
	-- Deposit all wood and saplings
	wood_chest:send("log")
	sapling_chest:send("sapling")
	
	-- Grab sticks and drop all junk
	sapling_chest:pull("stick")
	inventory.drop()
	
	-- Grab at most 64 wood and coal per furnace
	wood_chest:pull("log", nil, #furnaces * 64)
	coal_chest:pull("coal", nil, #furnaces * 64)
	
	-- Grab smelted charcoal from furnace, refill furnace with wood and coal
	for _, furnace_id in ipairs(furnaces) do
		local furnace = inventory.connect(furnace_id)
		furnace:pull(3)
		furnace:send("log", 1)
		furnace:send("coal", 2)
	end
	
	-- Deposit coal and wood again
	wood_chest:send("log")
	coal_chest:send("coal")
	
	-- Refuel if needed
	coal_chest:pull("coal", nil, inventory.get_fuel_needed(fuel_needed))
	inventory.refuel()
	
	-- Grab saplings
	sapling_chest:pull("sapling", nil, #trees)
end

-------------------------------------------

while true do
	deposit()
	
	for _, tree_position in ipairs(trees) do
		process_tree(tree_position)
	end
	
	deposit()
	
	-- Smelting a stack takes just a little over 10 minutes, so this is the most optimal:
	-- sleep(600)
	
	-- This looks cooler though
	-- sleep(60)
end
