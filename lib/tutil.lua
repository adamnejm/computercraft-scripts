
local move = require "lib.move"

-------------------------------------------

local function directional_action(target, action)
	if move.position.y < target.y then
		return action .. "Up"
	elseif move.position.y > target.y then
		return action .. "Down"
	else
		return action
	end
end

local function place_block(target)
	return turtle[directional_action(target, "place")]()
end

local function detect_block(target)
	return turtle[directional_action(target, "detect")]()
end

local function inspect_block(target)
	return turtle[directional_action(target, "inspect")]()
end

-------------------------------------------

return {
	directional_action = directional_action,
	place_block        = place_block,
	detect_block       = detect_block,
	inspect_block      = inspect_block,
}
