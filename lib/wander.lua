
local direction = true
local direction_functions = {
	[true]  = turtle.turnLeft,
	[false] = turtle.turnRight,
}

-------------------------------------------

return function()
	local turn = direction_functions[direction]
	if turtle.detect() then
		turn()
		turtle.forward()
		
		if turtle.detect() then
			turn()
		else
			turn()
			direction = not direction
		end
	else
		turtle.forward()
	end
end
