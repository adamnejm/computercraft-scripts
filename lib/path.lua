
local lazy   = require "lib.lazy"
local move   = require "lib.move"
local Vector = require "lib.vector"

-------------------------------------------

-- Discovered obstacles used by greedy best-first algorithm
local world = {}


-------------------------------------------
--                SIMPLE                 --
-------------------------------------------

local function go_to_along_x(target)
	if type(target) ~= "number" then target = target.x end
	while move.position.x ~= target do
		if not move.face_x(target) then return false end
		if not move.forward() then return false end
	end
	return true
end

local function go_to_along_z(target)
	if type(target) ~= "number" then target = target.z end
	while move.position.z ~= target do
		if not move.face_z(target) then return false end
		if not move.forward() then return false end
	end
	return true
end

local function go_to_along_y(target)
	if type(target) ~= "number" then target = target.y end
	local move_func = target > move.position.y and move.up or move.down
	while move.position.y ~= target do
		if not move_func() then return false end
	end
	return true
end

local go_to_axes = { go_to_along_x, go_to_along_y, go_to_along_z }
local function go_to_along_axes(target)
	for _, axis_func in ipairs(go_to_axes) do
		axis_func(target)
	end
	return move.position == target
end

-------------------------------------------

--- Very simple pathfinding that can only advance towards goal in descending manner
---
--- Will most likely fail if it encounters an obstacle
---@param target Vector Goal to reach
---@return boolean success Whether it reached the goal
local function towards(target)
	local min_distance = math.huge
	while true do
		go_to_along_axes(target)
		
		local distance = move.position:distance_sqr(target)
		if distance < min_distance then
			min_distance = distance
		else
			break
		end
	end
	return move.position == target
end


-------------------------------------------
--           GREEDY BEST-FIRST           --
-------------------------------------------

local function push_queue(queue, data)
	for i, v in ipairs(queue) do
		if v.cost > data.cost then
			table.insert(queue, i, data)
			return
		end
	end
	queue[#queue+1] = data
end

local function pop_queue(queue)
	return table.remove(queue, 1)
end

local function reconstruct_path(came_from, current)
	local path = { current }
	while current do
		current = came_from[tostring(current)]
		path[#path+1] = current
	end
	return path
end

-------------------------------------------

local function detect_world()
	world[tostring(move.position + move.NORMAL[move.direction])] = turtle.detect() or nil
	world[tostring(move.position + move.NORMAL[move.DIRECTION.UP])] = turtle.detectUp() or nil
	world[tostring(move.position + move.NORMAL[move.DIRECTION.DOWN])] = turtle.detectDown() or nil
end

local function go_to_neighbor(neighbor)
	local arrived = true -- May already be there
	if move.position.x ~= neighbor.x or move.position.z ~= neighbor.z then
		move.face(neighbor)
		arrived = move.forward()
	elseif move.position.y ~= neighbor.y then
		local move_func = neighbor.y > move.position.y and move.up or move.down
		arrived = move_func()
	end
	
	detect_world()
	return arrived
end

-------------------------------------------

-- Greedy best-first search, based on: https://en.wikipedia.org/wiki/Best-first_search#Greedy_BFS
local function greedy_best_first(start, target, limit)
	local queue, visited, came_from = {}, {}, {}
	
	push_queue(queue, { pos = start, cost = start:manhattan(target) })
	visited[tostring(start)] = true
	
	local expanded = 0
	while #queue > 0 do
		expanded = expanded + 1
		if expanded > limit then
			return false
		elseif expanded % 1000 == 0 then
			sleep(0) -- Yield so computercraft doesn't scream at us
		end
		
		local node = pop_queue(queue)
		
		-- Check vertical first since it doesn't require turning and generally going over an obstacle is faster anyway
		for i = #move.NORMAL, 1, -1 do
			local neighbor_pos  = node.pos + move.NORMAL[i]
			local neighbor_cost = neighbor_pos:manhattan(target)
			local neighbor_id   = tostring(neighbor_pos)
			
			if not visited[neighbor_id] and not world[neighbor_id] then
				came_from[neighbor_id] = node.pos
				
				if neighbor_pos == target then
					return reconstruct_path(came_from, target)
				else
					push_queue(queue, { pos = neighbor_pos, cost = neighbor_cost })
					visited[neighbor_id] = true
				end
			end
		end
	end
	
	return false
end

--- Pathfinds to the target position using the Greedy Best-First search algorithm
---@param target Vector Position to pathfind to
---@param retries? integer Number of retry attempts, 0 by default
---@param limit? integer Limit of expanded blocks, 50,000 by default
---@return boolean success True if arrived at the target
local function go_to_gbf(target, retries, limit)
	target:floor()
	if not limit then limit = 50000 end
	if not retries then retries = 0 end
	
	world = {}
	while move.position ~= target do
		local path = greedy_best_first(target, move.position, limit)
		if not path or world[tostring(target)] then
			if retries > 1 then
				return go_to_gbf(target, retries - 1, limit)
			else
				return false
			end
		end
		
		for i = 2, #path do
			if not go_to_neighbor(path[i]) then ---@diagnostic disable-line: need-check-nil
				break
			end
		end
	end
	
	return true
end

--- Pathfinds to any neighboring block of the target position and faces towards it using the Greedy Best-First algorithm
---@param target Vector Position to pathfind to
---@param retries? integer Number of retry attempts, 0 by default
---@param limit? integer Limit of expanded blocks, 50,000 by default
---@return boolean arrived True if arrived to a neighboring block and turned towards the goal
local function go_to_block_gbf(target, retries, limit)
	target:floor()
	if not limit then limit = 50000 end
	if not retries then retries = 0 end
	
	world = {}
	while move.position:manhattan(target) > 1 do
		local path = greedy_best_first(target, move.position, limit)
		if not path or world[tostring(target)] then
			if retries > 1 then
				return go_to_block_gbf(target, retries - 1, limit)
			else
				return false
			end
		end
		
		for i = 2, #path - 1 do
			if not go_to_neighbor(path[i]) then ---@diagnostic disable-line: need-check-nil
				break
			end
		end
	end
	
	if move.position == target then
		for i = #move.NORMAL, 1, -1 do
			if towards(move.position + move.NORMAL[i]) then
				break
			end
		end
	end
	
	return move.position:manhattan(target) == 1 and move.face(target)
end


-------------------------------------------
--               TRAVERSE                --
-------------------------------------------

--- Traverses a cuboid in the most efficient way using A* pathfinding algorithm
---@param mins Vector First corner of the cuboid
---@param maxs Vector Second corner of the cuboid
---@param callback function Function to call for every successfuly traversed block
local function traverse(mins, maxs, callback)
	local flip_x, flip_z
	for y = mins.y, maxs.y, lazy.math.sign(maxs.y - mins.y, true) do
		for z = mins.z, maxs.z, lazy.math.sign(maxs.z - mins.z, true) do
			local vz = flip_z and maxs.z + mins.z - z or z
			
			for x = mins.x, maxs.x, lazy.math.sign(maxs.x - mins.x, true) do
				local vx = flip_x and maxs.x + mins.x - x or x
				
				if go_to_gbf(Vector(vx, y, vz)) then
					callback()
				end
			end
			flip_x = not flip_x
		end
		flip_z = not flip_z
	end
end


-------------------------------------------
--                EXPORT                 --
-------------------------------------------

return {
	towards     = towards,
	go_to       = go_to_gbf,
	go_to_block = go_to_block_gbf,
	traverse    = traverse,
}
