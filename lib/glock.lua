
local basalt  = require "lib.basalt"
local lmath   = require "lib.lazy.math"
local lstring = require "lib.lazy.string"

-------------------------------------------

--[[ TODO:

V Add `error`, `warn`, `info`, etc.
V Add `nil` when using log() without any args
V Split long strings when printing and support newline character
V Add filtering support when scrolling
V Change LOG_LEVEL.NONE to LOG_LEVEL.DEBUG

X Change the newline behavior so we add newline after each line and maybe increase the height by 1?: Meh, the current solution is good enough
X Improve loopback, right now there are 2 separate receivers, thus problems with scrollback. Probably store the receiver in a GLOBAL and re-convert it when adding logger or receiver based on order of init: Meh, the loopback shouldn't have scrolling behavior anyway
X GUI

]]

-------------------------------------------

local LOG_LEVEL = {
	DEBUG = 0, [0] = "Debug",
	OK    = 1, [1] = "Okay",
	INFO  = 2, [2] = "Info",
	WARN  = 3, [3] = "Warning",
	ERROR = 4, [4] = "Error",
}

local Glock = {
	Logger    = {},
	Receiver  = {},
	Interface = {},
	LOG_LEVEL = LOG_LEVEL,
	
	rednet_protocol = "glock",
	
	theme = {
		[LOG_LEVEL.DEBUG]    = colors.lightGray,
		[LOG_LEVEL.OK]       = colors.green,
		[LOG_LEVEL.INFO]     = colors.white,
		[LOG_LEVEL.WARN]     = colors.orange,
		[LOG_LEVEL.ERROR]    = colors.red,
		["comma"]            = colors.gray,
		["generic"]          = colors.white,
		["nil"]              = colors.brown,
		["true"]             = colors.lime,
		["false"]            = colors.red,
		["number"]           = colors.lightBlue,
		["string"]           = colors.orange,
		["function"]         = colors.magenta,
		["object"]           = colors.yellow,
		["table"]            = colors.lightGray,
		["background"]       = colors.black,
		["computer_bracker"] = colors.white,
		["computer_color"]   = { -- Either color or a table to randomize based on computer ID
			colors.red,  colors.cyan,  colors.magenta, colors.yellow,
			colors.lime, colors.pink,  colors.orange,  colors.purple,
			colors.blue, colors.brown, colors.green,   colors.lightBlue,
		},
	},
}

local Logger, Receiver, Interface = Glock.Logger, Glock.Receiver, Glock.Interface
local rednet_protocol, theme = Glock.rednet_protocol, Glock.theme

-------------------------------------------

local function connect_to_rednet(is_required)
	peripheral.find("modem", rednet.open)
	if is_required and not rednet.isOpen() then
		error("Failed to open rednet. Glock requires a modem to function!")
	end
end

local function split(str, separator, use_patterns)
	local ret, pos = {}, 1
	for i = 1, string.len(str) do
		local start, finish = string.find(str, separator, pos, not use_patterns)
		if not start then
			break
		end
		
		ret[i] = string.sub(str, pos, start - 1)
		pos = finish + 1
	end
	
	ret[#ret+1] = string.sub(str, pos)
	return ret
end


-------------------------------------------
--                LOGGER                 --
-------------------------------------------

--- Creates a new logger object
---@param protocol? string Protocol
---@param category? string Category
---@param no_loopback? boolean True to disable printing to itself
---@param no_newline_fix? boolean True to not fix newline when printing to itself (may cause issues when also using default `print`)
function Glock.new_logger(protocol, category, no_loopback, no_newline_fix)
	local o = {
		protocol    = protocol or "General",
		category    = category or "General",
		newline_fix = not no_newline_fix, -- Only useful if you're planning to have a long-running receiver that's also supposed to self-log
	}
	
	if not no_loopback then
		local receiver = Glock.new_receiver(nil, nil, true)
		receiver:set_output(term, nil, nil, true)
		o.receiver = receiver
	end
	
	connect_to_rednet(false)
	return setmetatable(o, { __index = Logger, __call = function(self, ...) return self:log(...) end })
end

-------------------------------------------

--- Logs any number of arguments
---@param ... any Whatever you want
function Logger:log(...)
	local level = LOG_LEVEL.DEBUG
	local document = self:serialize_any(...)
	self:write(document, level)
end

--- Sends a success message
---@param message string Message in which %s are replaced with the vararg
---@param ... any Values to substitute in the message
function Logger:ok(message, ...)
	local level = LOG_LEVEL.OK
	local document = self:serialize_message(message, theme[level], ...)
	self:write(document, level)
end

--- Sends a regular message
---@param message string Message in which %s are replaced with the vararg
---@param ... any Values to substitute in the message
function Logger:info(message, ...)
	local level = LOG_LEVEL.INFO
	local document = self:serialize_message(message, theme[level], ...)
	self:write(document, level)
end

--- Sends a warning message
---@param message string Message in which %s are replaced with the vararg
---@param ... any Values to substitute in the message
function Logger:warn(message, ...)
	local level = LOG_LEVEL.WARN
	local document = self:serialize_message(message, theme[level], ...)
	self:write(document, level)
end

--- Sends an error message
---@param message string Message in which %s are replaced with the vararg
---@param ... any Values to substitute in the message
function Logger:error(message, ...)
	local level = LOG_LEVEL.ERROR
	local document = self:serialize_message(message, theme[level], ...)
	self:write(document, level)
end

-------------------------------------------

local document_types = {
	["nil"] = function(v)
		return { color = theme["nil"], value = "nil" }
	end,
	["generic"] = function(v)
		return { color = theme["generic"], value = tostring(v) }
	end,
	["boolean"] = function(v)
		return { color = v and theme["true"] or theme["false"], value = tostring(v) }
	end,
	["number"] = function(v)
		return { color = theme["number"], value = tostring(v) }
	end,
	["string"] = function(v)
		return { color = theme["string"], value = "\"" .. v .. "\"" }
	end,
	["function"] = function(v)
		return { color = theme["function"], value = tostring(v) }
	end,
	["table"] = function(v)
		local mt = getmetatable(v)
		if mt and mt.__tostring then
			return { color = theme["object"], value = tostring(v) }
		else
			return { color = theme["table"], value = tostring(v) }
		end
	end,
}

function Logger:serialize_any(...)
	local document = {}
	local length = select("#", ...)
	
	
	if length > 0 then
		for i = 1, length do
			local v = select(i, ...)
			local f = document_types[type(v)] or document_types["generic"]
			document[#document+1] = f(v)
			
			if i < length then
				document[#document+1] = { color = theme["comma"], value = ", " }
			end
		end
	else
		document[#document+1] = { color = theme["nil"], value = "nil" }
	end
	
	return document
end

function Logger:serialize_message(message, color, ...)
	local document = {}
	local explode = split(message, "%s")
	local length = #explode
	
	for i, sub in ipairs(explode) do
		document[#document+1] = { color = color, value = sub }
		local v = select(i, ...)
		if v then
			local f = document_types[type(v)] or document_types["generic"]
			document[#document+1] = f(v)
		elseif i < length then
			document[#document+1] = { color = theme["nil"], value = "nil" }
		end
	end
	
	return document
end

function Logger:write(document, level, protocol, category)
	local data = {
		document       = document,
		level          = level    or LOG_LEVEL.DEBUG,
		protocol       = protocol or self.protocol,
		category       = category or self.category,
		computer_id    = os.getComputerID(),    ---@diagnostic disable-line
		computer_label = os.getComputerLabel(), ---@diagnostic disable-line
	}
	
	rednet.broadcast(data, rednet_protocol)
	
	if self.receiver then
		local fix = self.newline_fix
		if fix then
			term.setCursorPos(1, select(2, term.getCursorPos()) - 1)
		end
		
		self.receiver:deserialize_document(data)
		
		if fix then
			print()
		end
	end
end


-------------------------------------------
--               RECEIVER                --
-------------------------------------------

function Glock.new_receiver(buffer_size, filter, is_loopback)
	local o = {
		filter = filter or {
			-- Filter examples:
			-- level       = {   [LOG_LEVEL.WARN] = true, [LOG_LEVEL.ERROR] = true },
			-- protocol    = {   ["Name"] = true,         ["Damianu"] = true },
			-- category    = {   ["Worker"] = true,       ["Storage"] = true },
			-- computer_id = {   [1] = true,              [2] = true },
		},
		output = output or term,
		-- width  = 102,
		-- height = 38,
		width  = 48,
		height = 16,
		ribbon = 30,
		scroll = 1,
		buffer = {},
		filtered_buffer = {},
		buffer_size = buffer_size or 100,
		is_loopback = is_loopback,
	}
	
	connect_to_rednet(not is_loopback)
	return setmetatable(o, { __index = Receiver })
end

function Receiver:check_filter(data)
	for property, filter in pairs(self.filter) do
		if not filter[data[property]] then
			return false
		end
	end
	return true
end

function Receiver:append_buffer(data, line)
	local passes_filter = self:check_filter(data)
	
	-- if not self.is_loopback then -- Dunno, about it, either way is fine I guess
		local buffer, filtered_buffer = self.buffer, self.filtered_buffer
		local buffer_size = self.buffer_size
		
		line.data = data
		
		table.insert(buffer, 1, line)
		if #buffer > buffer_size then
			buffer[#buffer] = nil
		end
		
		if passes_filter then
			table.insert(filtered_buffer, 1, line)
			if #filtered_buffer > buffer_size then
				filtered_buffer[#filtered_buffer] = nil
			end
		end
	-- end
	
	if passes_filter then
		if self.scroll == 1 then
			self:print_line(line, true)
		else
			self.scroll = self.scroll + 1
		end
	end
	
	return line
end

function Receiver:append_heading(data)
	local computer_id = data.computer_id
	local computer_label = string.gsub(data.computer_label, "%s*#%d*$", "") .. " #" ..computer_id
	
	local computer_color = theme["computer_color"]
	if type(computer_color) == "table" then
		computer_color = computer_color[computer_id % #computer_color + 1]
	end
	
	return {
		{ color = theme[data.level], value = "["            },
		{ color = computer_color,    value = computer_label },
		{ color = theme[data.level], value = "] "           },
		unpack(data.document),
	}
end

function Receiver:deserialize_document(data)
	local width, ribbon = self.width, self.ribbon
	local document = self:append_heading(data)
	
	local line, pos = {}, 0
	for i, item in ipairs(document) do
		local value = item.value
		local color = item.color
		local length = string.len(value)
		
		-- split by newlines
		local newline = string.find(value, "\n")
		if newline then
			table.insert(document, i + 1, { color = color, value = string.sub(value, newline + 1) })
			value = string.sub(value, 1, newline - 1)
		end
		
		if pos + length <= width then
			line[#line+1] = item
			pos = pos + length
		else
			-- flush previous if not enough space
			local space = width - pos
			if ribbon > space and #line > 0 then
				self:append_buffer(data, line)
				line, pos = {}, 0
				space = width - pos
			end
			
			-- submit part of current
			local new = string.sub(value, 1, space)
			line[#line+1] = { color = color, value = new }
			pos = pos + string.len(new)
			
			-- postpone reminder for next iteration
			table.insert(document, i + 1, { color = color, value = string.sub(value, space + 1) })
		end
		
		-- force push buffer after newline
		if newline then
			self:append_buffer(data, line)
			line, pos = {}, 0
		end
	end
	
	self:append_buffer(data, line)
end

function Receiver:listen()
	
	local function parallel_receive()
		while true do
			local computer_id, data = rednet.receive(rednet_protocol)
			data.computer_id = computer_id -- Reassign, because can be faked
			self:deserialize_document(data)
			
			if self.update_callback then
				self.update_callback("listen")
			end
		end
	end
	
	local function parallel_scroll()
		while true do
			local _, dir = os.pullEvent("mouse_scroll") ---@diagnostic disable-line
			self:set_scroll(self.scroll - dir)
		end
	end
	
	parallel.waitForAll(parallel_receive, parallel_scroll)
end

-------------------------------------------

function Receiver:print_line(line, do_scroll)
	local output = self.output
	
	if do_scroll then
		local y = select(2, output.getCursorPos())
		if y >= self.height then
			output.scroll(1)
			output.setCursorPos(1, self.height)
		else
			output.setCursorPos(1, y + 1)
		end
	end
	
	local previous_color = output.getTextColor()
	for _, item in ipairs(line) do
		output.setTextColor(item.color)
		output.write(item.value)
	end
	output.setTextColor(previous_color)
end

function Receiver:prepare_display()
	local output = self.output
	
	output.setBackgroundColor(theme["background"])
	output.clear()
	-- Y = 0 Because we don't insert a new line at the end of a line to make full use of the terminal
	output.setCursorPos(1, 0) -- TODO: Change Y to 0 as per desc above (right now it's 1 because 1-line errors do not get printed)
	
	-- TODO: Get term size here but watch out, `prepare_display` is not called on `loopback`
end

function Receiver:build_display()
	local output = self.output
	local scroll = self.scroll
	local previous_color = output.getTextColor()
	
	for i = 0, self.height - 1 do
		output.setCursorPos(1, self.height - i)
		self:print_line(self.filtered_buffer[i + scroll])
		
		-- Even `term.clearLine()` immediatelly followed by `term.write` causes blinking lines
		-- So instead, clear the remaining space using a character with the color of the background
		local x = output.getCursorPos()
		output.setTextColor(theme["background"])
		output.write(string.rep(" ", self.width - x + 1))
	end
	
	output.setTextColor(previous_color)
	output.setCursorPos(1, self.height)
	
	if self.update_callback then
		self.update_callback("build_display")
	end
end

-------------------------------------------

function Receiver:set_output(output, width, height, no_prepare)
	self.output = output
	
	local w, h = output:getSize()
	self.width  = width  or w
	self.height = height or h
	
	if not no_prepare then
		self:prepare_display()
	end
end

function Receiver:set_scroll(scroll)
	local height = self.height
	local length = #self.filtered_buffer
	if length <= height then return end
	
	-- scroll = math.min(math.max(scroll, height), math.max(length, math.min(height, length)))
	-- scroll = math.min(scroll, 1, math.max(length, length - self.height))
	scroll = lmath.clamp(scroll, 1, length - self.height + 1) -- TODO: +1 are we sure?
	if scroll ~= self.scroll then
		self.scroll = scroll
		self:build_display()
	end
end


-------------------------------------------
--               INTERFACE               --
-------------------------------------------

-- Work on interface is suspended, because I don't consider Basalt good enough
-- Maybe I'll do it, maybe not, there's very little use for the GUI anyway
-- Everything you need can be easily acomplished with 2 lines of code

-- function Glock.new_interface(...)
-- 	local o = {
-- 		receiver = Glock.new_receiver(...),
-- 		filter = {
-- 			level = {
-- 				-- enabled = false,
-- 				-- { name = LOG_LEVEL[LOG_LEVEL.NONE],  key = LOG_LEVEL.NONE,  value = false },
-- 				-- { name = LOG_LEVEL[LOG_LEVEL.OK],    key = LOG_LEVEL.OK,    value = false },
-- 				-- { name = LOG_LEVEL[LOG_LEVEL.INFO],  key = LOG_LEVEL.INFO,  value = false },
-- 				-- { name = LOG_LEVEL[LOG_LEVEL.WARN],  key = LOG_LEVEL.WARN,  value = false },
-- 				-- { name = LOG_LEVEL[LOG_LEVEL.ERROR], key = LOG_LEVEL.ERROR, value = false },
-- 			}
-- 		}
-- 	}
	
-- 	setmetatable(o, { __index = Interface })
	
-- 	o:create()
	
-- 	return o
-- end

-- -------------------------------------------

-- function Interface:create()
-- 	local frame = basalt.createFrame("Interface")
-- 	if not frame then error("oops") end
	
-- 	self:create_output_menu(frame)
-- 	-- self:create_filters_menu(frame)
-- 	self:create_header(frame)
-- 	-- self:create_anims(frame)
-- 	self.frame = frame
	
-- 	basalt.autoUpdate(true)
-- end

-- function Interface:create_output_menu(frame)
-- 	local receiver = self.receiver
-- 	local output, scrollbar
	
-- 	function receiver.update_callback()
-- 		scrollbar:setMaxValue(10)
-- 	end
	
	
-- 	lmath.clamp(scroll, 1, length - self.height + 1)
	
	
	
	
-- 	output = frame:addProgram()
-- 	output:setPosition(1, 2)
-- 	output:setSize("parent.w - 1", "parent.h - 1")
-- 	output:execute(function()
-- 		-- rednet.broadcast("rdy")
		
-- 		receiver:set_output(term)
		
-- 		rednet.broadcast(true, "rdy")
		
-- 		local function listen()
-- 			receiver:listen()
-- 		end
		
-- 		local function scroll()
-- 			while true do
-- 				local _, dir = os.pullEvent("mouse_scroll")
-- 				receiver:set_scroll(receiver.scroll - dir)
-- 			end
-- 		end
		
-- 		local function set_scroll()
-- 			while true do
-- 				local _, value = os.pullEvent("set_scroll")
-- 				receiver:set_scroll(value)
-- 			end
-- 		end
		
-- 		parallel.waitForAll(listen, scroll, set_scroll)
-- 	end)
	
-- 	scrollbar = frame:addScrollbar()
-- 	scrollbar:setPosition("parent.w", 2)
-- 	scrollbar:setSize(1, "parent.h - 1")
-- 	scrollbar:setForeground(colors.gray)
-- 	scrollbar:setBackground(colors.black)
-- 	scrollbar:setSymbolColor(colors.white)
-- 	scrollbar:setMaxValue(100)
-- 	scrollbar:onChange(function(s, value)
-- 		output:injectEvent("set_scroll", math.floor(value))
-- 	end)
	
	
-- 	frame.output_scrollbar = scrollbar
-- end

-- function Interface:create_filters_menu(frame)
-- 	local filters = frame:addFrame()
-- 	filters:setPosition(1, 2)
-- 	filters:setSize("parent.w", "parent.h - 1")
-- 	filters:setBackground(colors.lightGray)
-- 	frame.filters = filters
-- end

-- function Interface:create_anims(frame)
-- 	local easing, duration = "easeOutExpo", 0.3
	
-- 	local filters_anim = frame:addAnimation()
-- 	filters_anim:setMode(easing):setObject(frame.filters):move(1, 2, duration)
-- 	filters_anim:setMode(easing):setObject(frame.output):move(-frame.width + 1, 2, duration)
-- 	frame.filters.anim = filters_anim
	
-- 	local output_anim = frame:addAnimation()
-- 	output_anim:setMode(easing):setObject(frame.output):move(1, 2, duration)
-- 	output_anim:setMode(easing):setObject(frame.filters):move(frame.width + 1, 2, duration)
-- 	frame.output.anim = output_anim
-- end

-- function Interface:create_header(frame)
-- 	local header = frame:addFrame()
-- 	header:setPosition(1, 1)
-- 	header:setSize("parent.w + 1", 1)
-- 	header:setBackground(colors.cyan)
-- 	frame.header = header
	
-- 	local header_info = header:addLabel()
-- 	header_info:setText("Glock v1.2 by Name")
-- 	header_info:setPosition("parent.w / 2 - self.w / 2", 1)
-- 	header_info:setForeground(colors.gray)
	
-- 	local header_clock = header:addLabel()
-- 	header_clock:setText("00:00:00")
-- 	header_clock:setForeground(colors.gray)
	
-- 	local header_clock_timer = header:addTimer()
-- 	local function header_clock_timer_update()
-- 		-- header_clock:setText(os.date("%H:%M:%S"))
-- 		-- header_clock:setPosition("parent.w - 8", 1)
-- 		-- header_clock_timer:start()
-- 	end
-- 	header_clock_timer_update()
-- 	header_clock_timer:setTime(1):start()
-- 	header_clock_timer:onCall(header_clock_timer_update)
	
-- 	local output_tab = header:addButton()
-- 	output_tab:setPosition(1, 1)
-- 	output_tab:setSize(10, 1)
-- 	output_tab:setText("Output")
-- 	output_tab:setForeground(colors.white)
-- 	output_tab:setBackground(colors.blue)
-- 	output_tab:onClick(function(_, event)
-- 		if event == "mouse_click" then
-- 			self:select_output()
-- 		end
-- 	end)
-- 	header.output_tab = output_tab
	
-- 	local filters_tab = header:addButton()
-- 	filters_tab:setPosition(11, 1)
-- 	filters_tab:setSize(11, 1)
-- 	filters_tab:setText("Filters")
-- 	filters_tab:setForeground(colors.gray)
-- 	filters_tab:setBackground(colors.lightBlue)
-- 	filters_tab:onClick(function(_, event)
-- 		if event == "mouse_click" then
-- 			self:select_filters()
-- 		end
-- 	end)
-- 	header.filters_tab = filters_tab
	
-- 	return header
-- end

-- -------------------------------------------

-- function Interface:select_output()
-- 	local output_tab = self.frame.header.output_tab
-- 	output_tab:setForeground(colors.white)
-- 	output_tab:setBackground(colors.blue)
	
-- 	local filters_tab = self.frame.header.filters_tab
-- 	filters_tab:setForeground(colors.gray)
-- 	filters_tab:setBackground(colors.lightBlue)
	
-- 	self:apply_filters()
	
-- 	self.frame.output.anim:play()
-- end

-- function Interface:select_filters()
-- 	local output_tab = self.frame.header.output_tab
-- 	output_tab:setForeground(colors.gray)
-- 	output_tab:setBackground(colors.lightBlue)
	
-- 	local filters_tab = self.frame.header.filters_tab
-- 	filters_tab:setForeground(colors.white)
-- 	filters_tab:setBackground(colors.blue)
	
-- 	self:update_filters()
	
-- 	self.frame.filters.anim:play()
-- end

-- -------------------------------------------

















-- function Interface:update_filters()
-- 	local receiver_filter = self.receiver.filter
	
-- 	-- Level
-- 	if receiver_filter.level then
		
-- 	end
-- end




-- function Interface:apply_filters()
-- 	local receiver_filter = self.receiver.filter
-- 	local interface_filter = self.filter
	
-- 	-- Level
-- 	if interface_filter.level.enabled then
-- 		receiver_filter.level = {}
-- 		for k, v in ipairs(self.filter.level) do
-- 			interface_filter.level[k] = v.value
-- 		end
-- 	else
-- 		receiver_filter.level = nil
-- 	end
-- end


-- -------------------------------------------





-- -- function Interface:create_filter(name)
-- -- 	local filters_frame = self.frame.filters
-- -- 	local filter = self.filter[name]
	
	
-- -- 	if not filters_frame.level then
-- -- 		subframe = frame:addFrame()
-- -- 		subframe:setPosition(2, 2)
-- -- 		subframe:setBackground(colors.lightGray)
-- -- 		subframe:setSize(20, "parent.h - 2")
-- -- 		frame[name] = subframe
		
		
		
-- -- 	end
	
	
-- -- 	if not frame.title then
-- -- 		local title = subframe:addButton()
-- -- 		title:setText("Level")
-- -- 		title:setSize("parent.w", 3)
-- -- 		title:setForeground(colors.white)
-- -- 		title:setBackground(colors.blue)
-- -- 		frame.title = title
-- -- 	end
	
-- -- 	if frame.items then
-- -- 		basalt.removeFrame(frame.items:getName())
-- -- 	end
-- -- 	local items = subframe:addFrame()
-- -- 	items:setPosition(1, 5)
-- -- 	frame.items = items
	
-- -- 	for i, v in ipairs(filter) do
-- -- 		local indicator = items:addPane()
-- -- 		indicator:setPosition(1, i)
-- -- 		indicator:setSize(1, 1)
-- -- 		indicator:setBackground(v.value and colors.lime or colors.red)
		
-- -- 		local btn = items:addLabel()
-- -- 		btn:setPosition(2, i)
-- -- 		btn:setSize("parent.w - 1")
-- -- 		btn:setText(" " .. v.name)
-- -- 		btn:setForeground(v.value and colors.white or colors.black)
-- -- 		btn:setBackground(colors.gray)
		
-- -- 		btn:onClick(function()
-- -- 			local value = not v.value
-- -- 			v.value = value
-- -- 			indicator:setBackground(value and colors.lime or colors.red)
-- -- 			btn:setForeground(value and colors.white or colors.black)
-- -- 		end)
		
-- -- 		filter.button = btn
-- -- 	end
-- -- end


-------------------------------------------
--                EXPORT                 --
-------------------------------------------

return Glock
