
local lazy   = require "lib.lazy"
local Vector = require "lib.vector"

-------------------------------------------

---@enum DIRECTION
local DIRECTION = {
	NONE  = 0, [0] = "None",  -- Unknown
	NORTH = 1, [1] = "North", -- -Z
	EAST  = 2, [2] = "East",  -- +X
	SOUTH = 3, [3] = "South", -- +Z
	WEST  = 4, [4] = "West",  -- -X
	UP    = 5, [5] = "Up",    -- +Y
	DOWN  = 6, [6] = "Down",  -- -Y
}

---@enum NORMAL
local NORMAL = {
	[DIRECTION.NONE]  = Vector( 0,  0,  0), NONE  = Vector( 0,  0,  0),
	[DIRECTION.NORTH] = Vector( 0,  0, -1), NORTH = Vector( 0,  0, -1),
	[DIRECTION.EAST]  = Vector( 1,  0,  0), EAST  = Vector( 1,  0,  0),
	[DIRECTION.SOUTH] = Vector( 0,  0,  1), SOUTH = Vector( 0,  0,  1),
	[DIRECTION.WEST]  = Vector(-1,  0,  0), WEST  = Vector(-1,  0,  0),
	[DIRECTION.UP]    = Vector( 0,  1,  0), UP    = Vector( 0,  1,  0),
	[DIRECTION.DOWN]  = Vector( 0, -1,  0), DOWN  = Vector( 0, -1,  0),
}

local turtle_forward    = turtle.forward
local turtle_back       = turtle.back
local turtle_up         = turtle.up
local turtle_down       = turtle.down
local turtle_turn_left  = turtle.turnLeft
local turtle_turn_right = turtle.turnRight

-------------------------------------------

---@class Move
---@field position Vector Current position of the turtle
---@field direction DIRECTION Current direction of the turtle
local move = {
	position  = Vector(0, 0, 0),
	direction = DIRECTION.NONE,
}


-------------------------------------------
--               MOVEMENT                --
-------------------------------------------

--- Moves the turtle forward
---@return boolean success
local function forward()
	if turtle_forward() then
		move.position:add(NORMAL[move.direction])
		return true
	end
	return false
end

--- Moves the turtle back
---@return boolean success
local function back()
	if turtle_back() then
		move.position:sub(NORMAL[move.direction])
		return true
	end
	return false
end

--- Moves the turtle up
---@return boolean success
local function up()
	if turtle_up() then
		move.position:add(NORMAL[DIRECTION.UP])
		return true
	end
	return false
end

--- Moves the turtle down
---@return boolean success
local function down()
	if turtle_down() then
		move.position:add(NORMAL[DIRECTION.DOWN])
		return true
	end
	return false
end

--- Turns the turtle to the left
---@return boolean success
local function left()
	if turtle_turn_left() then
		move.direction = lazy.math.wrap(move.direction - 1, 1, 4)
		return true
	end
	return false
end

--- Turns the turtle to the right
---@return boolean success
local function right()
	if turtle_turn_right() then
		move.direction = lazy.math.wrap(move.direction + 1, 1, 4)
		return true
	end
	return false
end

--- Turns the turtle to the specified direction in the most optimal way
---@param target_direction DIRECTION Direction to turn towards
---@return boolean success
local function turn(target_direction)
	if move.direction ~= target_direction then
		local clockwise = lazy.math.wrap(target_direction - move.direction, 1, 4) <= 2
		local turn_func = clockwise and right or left
		while move.direction ~= target_direction do
			if not turn_func() then
				return false
			end
		end
	end
	return true
end

--- Moves the turtle towards the specified direction, turning beforehand if necessary
---@param target_direction DIRECTION Direction to move towards
---@return boolean success
local function towards(target_direction)
	if target_direction == DIRECTION.UP then
		return up()
	elseif target_direction == DIRECTION.DOWN then
		return down()
	else
		if turn(target_direction) then
			return forward()
		end
	end
	return false
end

--- Turns the turtle towards the block on X axis
---@param target Vector|number Vector position or number X component of the position
---@return boolean success
local function face_x(target)
	if type(target) ~= "number" then target = target.x end
	if target > move.position.x then
		return turn(DIRECTION.EAST)
	elseif target < move.position.x then
		return turn(DIRECTION.WEST)
	end
	return true
end

--- Turns the turtle towards the block on Z axis
---@param target Vector|number Vector position or number Z component of the position
---@return boolean success
local function face_z(target)
	if type(target) ~= "number" then target = target.z end
	if target > move.position.z then
		return turn(DIRECTION.SOUTH)
	elseif target < move.position.z then
		return turn(DIRECTION.NORTH)
	end
	return true
end

--- Turns the turtle to face the specific direction, first on the X axis, then the Z axis
---@param target Vector
---@return boolean success
local function face(target)
	if face_x(target) then
		return face_z(target)
	end
	return false
end


-------------------------------------------
--               LOCATION                --
-------------------------------------------

--- Updates the position of the turtle and returns a copy of the acquired Vector
---@return Vector position
local function locate()
	local x, y, z = gps.locate(0)
	if not x then
		print("Location service unavailable! Waiting...")
		repeat
			x, y, z = gps.locate(0)
			sleep(5)
		until x
	end
	
	move.position = Vector(x, y, z)
	return move.position:clone()
end

--- Calibrates the turtle's position and direction
---
--- In order to aquire the direction, turtle needs to move one block horizontally, it will travel up or down if needed
---
--- If the turtle cannot move forward or back, the program will error with position of the stuck turtle
---@param return_to_start? boolean Whether to try and return to position prior to calibration. True by default
---@return boolean calibration_success Correct position and direction has been acquired
---@return boolean returned_to_start Turtle has reached its starting position. Always False if `return_to_start` is False
local function calibrate(return_to_start)
	local previous_position = locate()
	
	local forward_multiplier = 0
	local function try_to_move_horizontally()
		if forward() then
			forward_multiplier = 1
			return true
		elseif back() then
			forward_multiplier = -1
			return true
		end
	end
	
	local can_move_up = true
	local travelled_vertically = 0
	local function try_to_move_vertically()
		if can_move_up then
			if up() then
				travelled_vertically = travelled_vertically + 1
			else
				can_move_up = false
				for _ = 1, travelled_vertically + 1 do
					down()
				end
				travelled_vertically = -1
			end
		else
			if down() then
				travelled_vertically = travelled_vertically - 1
			else
				error(string.format("Calibration failed, turtle is stuck at %s with fuel level of %s!", move.position, turtle.getFuelLevel()))
			end
		end
	end
	
	local turn_amount = 0
	local function try_to_turn()
		if right() then
			turn_amount = turn_amount + 1
		else
			error("Damianu zasysa zielone siury ufoludków")
		end
	end
	
	local function try_to_return_to_start()
		if forward_multiplier > 0 then
			if not back() then return false end
		elseif forward_multiplier < 0 then
			if not forward() then return false end
		end
		
		if turn_amount > 0 then
			if not turn(lazy.math.wrap(move.direction - turn_amount, 1, 4)) then return false end
		end
		
		local move_func = travelled_vertically > 0 and down or up
		for _ = 1, math.abs(travelled_vertically) do
			if not move_func() then return false end
		end
		return true
	end
	
	-------------------------------------------
	
	while true do
		if try_to_move_horizontally() then break end
		try_to_turn()
		if try_to_move_horizontally() then break end
		try_to_move_vertically()
	end
	
	locate()
	local position_delta = (move.position - previous_position):set_y(0) * forward_multiplier
	
	for normal_direction, normal in ipairs(NORMAL) do
		if normal == position_delta then
			move.direction = normal_direction
		end
	end
	
	local did_return_to_start = false
	if return_to_start or return_to_start == nil then
		did_return_to_start = try_to_return_to_start()
	end
	
	print(string.format("Calibrated to %s facing %s", move.position, DIRECTION[move.direction]))
	return true, did_return_to_start
end


-------------------------------------------
--                EXPORT                 --
-------------------------------------------

turtle.forward   = forward
turtle.back      = back
turtle.up        = up
turtle.down      = down
turtle.turnLeft  = left
turtle.turnRight = right

move.DIRECTION   = DIRECTION
move.NORMAL      = NORMAL

move.forward     = forward
move.back        = back
move.up          = up
move.down        = down
move.towards     = towards
move.left        = left
move.right       = right
move.turn        = turn
move.face_x      = face_x
move.face_z      = face_z
move.face        = face

move.locate      = locate
move.calibrate   = calibrate

-------------------------------------------

calibrate(true)

return move
