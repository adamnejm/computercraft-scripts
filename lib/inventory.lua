
local lazy = require "lib.lazy"

-------------------------------------------

local TURTLE_SIZE = 16
local FUEL_TYPES = {
	["minecraft:coal"] = 80,
	["minecraft:charcoal"] = 80,
}
local FUEL_ITEMS = {
	"minecraft:coal",
	"minecraft:charcoal",
}

-------------------------------------------

local function matches(name, data, exact, slot)
	if not data then return end
	if name == nil then
		return data
	elseif name == false then
		return not data
	elseif type(name) == "number" then
		return name == slot
	elseif type(name) == "table" then
		for _, v in ipairs(name) do
			if exact then
				if data.name == v then
					return true
				end
			else
				if string.find(data.name, v) then
					return true
				end
			end
		end
	elseif exact then
		return data.name == name
	else
		return string.find(data.name, name)
	end
end

-------------------------------------------

local function get_size()
	return TURTLE_SIZE
end

--- Returns total item count in turtle's inventory
---@param name string|table|false|nil String name of the item, Array of item names, False to find empty or `nil` to match all
---@param exact? boolean True to ensure match the name exactly
---@return integer count Total count of found items
local function get_count(name, exact)
	local count = 0
	for i = 1, TURTLE_SIZE do
		local data = turtle.getItemDetail(i)
		if matches(name, data, exact, i) then
			count = count + data.count
		end
	end
	return count
end

--- Returns a table of items all items in the storage
---@return table<number, table> slots Map of all items, where key is the slot and value is item data, ie. name of the item
local function get_items()
	local slots = {}
	for i = 1, TURTLE_SIZE do
		local data = turtle.getItemDetail(i)
		if data then
			slots[i] = data
		end
	end
	return slots
end

--- Searches for an item by name in turtle's inventory
---@param name string|table|false|nil String name of the item, Array of item names, False to find empty or `nil` to match all
---@param exact? boolean True to ensure match the name exactly
---@return integer? slot Slot of the item, if any
---@return string? name Name of the item, if any
---@return integer? count Count of the items, if any
local function find(name, exact)
	for i = 1, TURTLE_SIZE do
		local data = turtle.getItemDetail(i)
		if matches(name, data, exact, i) then
			return i, data.name, data.count
		end
	end
end

--- Searches for multiple items by name in turtle's inventory
---@param name string|table|false|nil String name of the item, Array of item names, False to find empty or `nil` to match all
---@param exact? boolean True to ensure match the name exactly
---@return table items Array of items, each entry containing following keys: `slot`, `name`, `count`
---@return integer count Total count of found items
local function find_all(name, exact)
	local result, total_count = {}, 0
	for i = 1, TURTLE_SIZE do
		local data = turtle.getItemDetail(i)
		if matches(name, data, exact, i) then
			total_count = total_count + data.count
			result[#result+1] = { slot = i, name = data.name, count = data.count }
		end
	end
	return result, total_count
end

---@alias drop_direction
---|"up" Drop items up
---|"down" Drop items down
---|"front" Drop items in front
local drop_functions = {
	up    = turtle.dropUp,
	down  = turtle.dropDown,
	front = turtle.drop,
}

--- Drops items from turtle's inventory
---@param name string|table|false|nil String name of the item, Array of item names, False to find empty or `nil` to match all
---@param limit? integer Maximum amount of items to drop
---@param direction? drop_direction Direction to drop in, up by default
---@param exact? boolean True to ensure match the name exactly
---@return integer|false total_count Total count of dropped items or False if none dropped
local function drop(name, limit, direction, exact)
	local total_count = 0
	local drop_function = drop_functions[direction] or drop_functions.up
	for i = 1, TURTLE_SIZE do
		local data = turtle.getItemDetail(i)
		if matches(name, data, exact, i) then
			local drop_amount = limit and math.min(limit - total_count, data.count) or data.count
			if turtle.select(i) and drop_function(drop_amount) then
				total_count = total_count + drop_amount
			end
		end
	end
	return total_count > 0 and total_count
end

--- Returns information about the currently selected item
---@return number slot Selected slot
---@return string? name Name of the selected item
---@return number? count Count of the selected item
local function get_selected()
	local slot = turtle.getSelectedSlot()
	local data = turtle.getItemDetail(slot)
	if data then
		return slot, data.name, data.count
	else
		return slot
	end
end

--- Selects an item in turtle's inventory
---@param name string|number|table|false|nil String name of the item, Number index of the slot, Array of item names, False to find empty or `nil` to match all
---@param exact? boolean True to ensure match the name exactly
---@return boolean? selected True if it has been found and selected
local function select(name, exact)
	if type(name) == "number" then
		return turtle.select(name)
	else
		local slot = find(name, exact)
		if slot then
			return turtle.select(slot)
		end
	end
end

-------------------------------------------

--- Returns current fuel level of the turtle
---@return number fuel Amount of fuel
local function get_fuel_level()
	return turtle.getFuelLevel()
end

--- Returns maximum amount of fuel the turtle can store
---@return number fuel_limit Maximum amount of fuel
local function get_fuel_limit()
	return turtle.getFuelLimit()
end

--- Returns the amount of fuel items needed to reach a specific fuel level
---@param fuel_level integer Target fuel level to reach
---@param fuel_cost? integer|string Number fuel cost per item, String to lookup fuel cost from `inventory.FUEL_TYPES`. Uses coal (80) by default
local function get_fuel_needed(fuel_level, fuel_cost)
	if not fuel_cost then
		fuel_cost = FUEL_TYPES["minecraft:coal"]
	elseif type(fuel_cost) == "string" then
		fuel_cost = FUEL_TYPES[fuel_cost]
	end
	
	return math.max(0, math.ceil((fuel_level - get_fuel_level()) / fuel_cost))
end

--- Scans turtle's inventory for fuel and consumes it
---@param amount? integer Amount of items to consume, will cosume everything it can by default
---@param fuel_types? table<string, true> Lookup table of allowed fuel types, by default only allows `minecraft:coal` and `minecraft:charcoal`
---@return boolean refueled True if has refueled, false otherwise
local function refuel(amount, fuel_types)
	if amount and amount <= 0 then return false end
	if not fuel_types then fuel_types = FUEL_TYPES end
	
	local refueled = false
	for i = 1, TURTLE_SIZE do
		local data = turtle.getItemDetail(i)
		if data and fuel_types[data.name] then
			local count = data.count
			
			turtle.select(i)
			if turtle.refuel(amount) then
				refueled = true
				
				if amount then
					amount = amount - count
					if amount <= 0 then
						return true
					end
				end
			else
				break
			end
		end
	end
	return refueled
end

-------------------------------------------

---@class Storage
---@field hub table Wrapped Wired Modem
---@field inv table Wrapped storage inventory
local Storage = {}

--- Connects to a storage via Wired Modem hub
---@param id? string|number Name of the storage or its ID, otherwise it'll connect to any available chest
---@return Storage
local function connect(id)
	local hub, inv
	for _, v in ipairs(peripheral.getNames()) do
		local _, class = peripheral.getType(v)
		if class == "peripheral_hub" then
			hub = peripheral.wrap(v)
			hub = peripheral.wrap(v)
		elseif class == "inventory" and id == nil or v == id or type(id) == "number" and tonumber(string.sub(v, -string.len(id))) == id then
			inv = peripheral.wrap(v)
		end
	end
	
	assert(hub, "Cannot find a hub to connect to!")
	assert(inv, "Cannot find a storage to connect to!")
	
	-- If the turtle has just came up to the hub, it may take some time for it to register, usually ~0.1s
	while not hub.getNameLocal() do
		sleep(0.1)
	end
	
	return setmetatable({ hub = hub, inv = inv }, { __index = Storage })
end

--- Returns total amount of slots in the storage
---@return number slots Storage size
function Storage:get_size()
	return self.inv.size()
end

--- Returns a table of items all items in the storage
---@return table<number, table> slots Map of all items, where key is the slot and value is item data, ie. name of the item
function Storage:get_items()
	return self.inv.list()
end

--- Searches for an item by name in the storage
---@param name string|table|false|nil String name of the item, Array of item names, False to find empty or `nil` to match all
---@param exact? boolean True to ensure match the name exactly
---@return integer? slot Slot of the item, if any
---@return string? name Name of the item, if any
---@return integer? count Count of the items, if any
function Storage:find(name, exact)
	for i, data in pairs(self.inv.list()) do
		if matches(name, data, exact, i) then
			return i, data.name, data.count
		end
	end
end

--- Searches for multiple items by name in the storage
---@param name string|table|false|nil String name of the item, Array of item names, False to find empty or `nil` to match all
---@param exact? boolean True to ensure match the name exactly
---@return table items Array of items, each entry containing following keys: `slot`, `name`, `count`
---@return integer count Total count of found items
function Storage:find_all(name, exact)
	local result, total_count = {}, 0
	for i, data in pairs(self.inv.list()) do
		if matches(name, data, exact, i) then
			total_count = total_count + data.count
			result[#result+1] = { slot = i, name = data.name, count = data.count }
		end
	end
	return result, total_count
end

--- Returns total item count in the storage
---@param name string|table|false|nil String name of the item, Array of item names, False to find empty or `nil` to match all
---@param exact? boolean True to ensure match the name exactly
---@return integer count Total count of found items
function Storage:get_count(name, exact)
	local count = 0
	for i, data in pairs(self.inv.list()) do
		if matches(name, data, exact, i) then
			count = count + data.count
		end
	end
	return count
end

--- Sends items from turtle to the storage
---@param name string|number|table|false|nil String name of the item, Number slot index, Array of item names, False to find empty or `nil` to match all
---@param slot? integer Slot index to send items to
---@param limit? integer Maximum amount of items to send
---@param reach? boolean Whether to only return True if transferred count matches the limit exactly
---@param exact? boolean True to ensure match the name exactly
---@return boolean transferred_enough Whether enough items have been transfered, if `exact` option is used it the count must match the limit, otherwise at least 1 must be sent
---@return integer transferred_count Total amount of transferred items
function Storage:send(name, slot, limit, reach, exact)
	if limit == 0 then return true, 0 end
	
	local transferred_count = 0
	for i = 1, TURTLE_SIZE do
		local data = turtle.getItemDetail(i)
		if matches(name, data, exact, i) then
			local count = self.inv.pullItems(self.hub.getNameLocal(), i, limit and limit - transferred_count, slot)
			transferred_count = transferred_count + count
			
			if count == 0 then break end
		end
	end
	
	if reach then
		return limit == transferred_count, transferred_count
	else
		return transferred_count > 0, transferred_count
	end
end

--- Pulls items from storage into turtle's inventory
---@param name string|number|table|false|nil String name of the item, Number slot index, Array of item names, False to find empty or `nil` to match all
---@param slot? integer Slot index to pull items to
---@param limit? integer Maximum amount of items to pull
---@param reach? boolean Whether to only return True if transferred count matches the limit exactly
---@return boolean transferred_enough Whether enough items have been transfered, if `exact` option is used it the count must match the limit, otherwise at least 1 must be sent
---@return integer transferred_count Total amount of transferred items
function Storage:pull(name, slot, limit, reach, exact)
	if limit == 0 then return true, 0 end
	
	local transferred_count = 0
	for i, data in pairs(self.inv.list()) do
		if matches(name, data, exact, i) then
			local count = self.inv.pushItems(self.hub.getNameLocal(), i, limit and limit - transferred_count, slot)
			transferred_count = transferred_count + count
			
			if count == 0 then break end
		end
	end
	
	if reach then
		return limit == transferred_count, transferred_count
	else
		return transferred_count > 0, transferred_count
	end
end

-------------------------------------------

return {
	TURTLE_SIZE     = TURTLE_SIZE,
	FUEL_TYPES      = FUEL_TYPES,
	FUEL_ITEMS      = FUEL_ITEMS,
	
	get_size        = get_size,
	get_count       = get_count,
	get_items       = get_items,
	find            = find,
	find_all        = find_all,
	drop            = drop,
	get_selected    = get_selected,
	select          = select,
	
	get_fuel_level  = get_fuel_level,
	get_fuel_limit  = get_fuel_limit,
	get_fuel_needed = get_fuel_needed,
	refuel          = refuel,
	
	connect         = connect,
}
