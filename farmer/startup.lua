
local wander    = require "lib.wander"
local inventory = require "lib.inventory"

-------------------------------------------

local fuel_needed = 1000

local produce_items = {
	"minecraft:wheat",
	"minecraft:potato",
	"minecraft:carrot",
	"minecraft:beetroot",
}

local seed_items = {
	"minecraft:wheat_seeds",
	"minecraft:beetroot_seeds",
}

-------------------------------------------

local last_seed = "minecraft:wheat_seeds"
local function cultivate(name)
	if name then
		last_seed = name
	end
	
	turtle.digDown() -- This also tills the dirt
	if inventory.select(name or last_seed, true) then
		turtle.placeDown()
		turtle.select(1)
	end
end

-------------------------------------------

local procedures = {
	["minecraft:air"] = function()
		cultivate()
	end,
	
	["minecraft:carrots"] = function(data)
		if data.state.age == 7 then
			cultivate("minecraft:carrot")
		end
	end,
	
	["minecraft:potatoes"] = function(data)
		if data.state.age == 7 then
			cultivate("minecraft:potato")
		end
	end,
	
	["minecraft:beetroots"] = function(data)
		if data.state.age == 3 then
			cultivate("minecraft:beetroot_seeds")
		end
	end,
	
	["minecraft:wheat"] = function(data)
		if data.state.age == 7 then
			cultivate("minecraft:wheat_seeds")
		end
	end,
	
	["computercraft:wired_modem_full"] = function()
		local produce_chest = inventory.connect(28)
		produce_chest:send(produce_items, nil, nil, nil, true)
		
		local seeds_chest = inventory.connect(30)
		seeds_chest:send(seed_items, nil, nil, nil, true)
		
		inventory.drop() -- Drop the remaining items
		
		if inventory.get_fuel_level() < fuel_needed then
			print("Waiting for fuel...")
			repeat
				sleep(5)
				local fuel_chest = inventory.connect(29)
				fuel_chest:pull(inventory.FUEL_ITEMS, nil, math.ceil((fuel_needed - inventory.get_fuel_level()) / 80))
				inventory.refuel()
			until inventory.get_fuel_level() >= fuel_needed
		end
	end,
}

-------------------------------------------

while true do
	local block_exists, block_data = turtle.inspectDown()
    if not block_exists then
        procedures["minecraft:air"]()
    elseif procedures[block_data.name] then
        procedures[block_data.name](block_data)
    end
	
	wander()
end
