
local wander = require "lib.wander"
local inventory = require "lib.inventory"

-------------------------------------------

local fuel_needed = 200

-------------------------------------------

local function sheer()
	if not inventory.select("shears") then return end
	turtle.placeDown()
end

local function manage_inventory()
	local block_exists, block_data = turtle.inspectUp()
	if not block_exists or block_data.name ~= "computercraft:wired_modem_full" then return end
	
	local wool_chest = inventory.connect(32)
	wool_chest:send("wool")
	
	if not inventory.find("shears") then
		print("Waiting for shears...")
		repeat
			sleep(5)
			local shears_chest = inventory.connect(31)
			shears_chest:pull("shears", nil, 1)
		until inventory.find("shears")
	end
	
	if inventory.get_fuel_level() < fuel_needed then
		print("Waiting for fuel...")
		repeat
			sleep(5)
			local fuel_chest = inventory.connect(31)
			fuel_chest:pull(inventory.FUEL_ITEMS, nil, math.ceil((fuel_needed - inventory.get_fuel_level()) / 80))
			inventory.refuel()
		until inventory.get_fuel_level() >= fuel_needed
	end
end

-------------------------------------------

while true do
	manage_inventory()
	sheer()
	wander()
end
